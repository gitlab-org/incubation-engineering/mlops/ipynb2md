package ipynb2md

import (
	"bytes"
	"embed"
	_ "embed"
	"encoding/json"
	"errors"
	"regexp"
	"strings"
	"text/template"

	"gopkg.in/yaml.v2"
)

//go:embed templates/*
var content embed.FS

type Notebook struct {
	Cells    []Cell           `json:"cells"`
	Metadata NotebookMetadata `json:"metadata"`
	NbFormat int              `json:"nbformat"`
	NbFormatMinor int              `json:"nbformat_minor"`
}

type Cell struct {
	CellType       string       `json:"cell_type"`
	Id             string       `json:"id"`
	Metadata       CellMetadata `json:"metadata"`
	Source         []string     `json:"source"`
	ExecutionCount int32        `json:"execution_count"`
	Outputs        []Output     `json:"outputs"`
}

type Output struct {
	ExecutionCount int32                  `json:"execution_count"`
	OutputType     string                 `json:"output_type"`
	Metadata       map[string]string      `json:"metadata"`
	Data           map[string]interface{} `json:"data"`
	Traceback      []string               `json:"traceback"`
}

type CellMetadata struct {
	Tags []string `json:"tags"`
}

type NotebookMetadata struct {
	Kernelspec   Kernelspec   `json:"kernelspec"`
	LanguageInfo LanguageInfo `json:"language_info"`
}

type Kernelspec struct {
	DisplayName string `json:"display_name" yaml:"display_name"`
	Language    string `json:"language" yaml:"language"`
	Name        string `json:"name" yaml:"name"`
}
type CodemirrorMode struct {
	Name    string `json:"name" yaml:"name"`
	Version int    `json:"version" yaml:"version"`
}
type LanguageInfo struct {
	CodemirrorMode CodemirrorMode `json:"codemirror_mode" yaml:"codemirror_mode"`
	FileExtension  string         `json:"file_extension" yaml:"file_extension"`
	Mimetype          string         `json:"mimetype" yaml:"mimetype"`
	Name              string         `json:"name" yaml:"name"`
	NbconvertExporter string         `json:"nbconvert_exporter" yaml:"nbconvert_exporter"`
	PygmentsLexer     string         `json:"pygments_lexer" yaml:"pygments_lexer"`
	Version           string         `json:"version" yaml:"version"`
}

type JupyterConf struct {
	Jupyter Jupyter `yaml:"jupyter"`
}

type Jupyter struct {
	Kernelspec   Kernelspec   `yaml:"kernelspec"`
	LanguageInfo LanguageInfo `yaml:"language_info"`
	Nbformat     int          `yaml:"nbformat"`
	NbformatMinor int          `yaml:"nbformat_minor"`
}

func FormatError(str string) string {
	stripped := regexp.MustCompile("[[0-9][0-9;]*m").ReplaceAllString(str, "")
	var lines []string
	for _, line := range strings.Split(stripped, "\n") {
		line = strings.Replace(line, "\u001B", "    ", 1)
		line = strings.ReplaceAll(line, "\u001B", "")
		lines = append(lines, line)
	}

	return strings.Join(lines, "\n")
}

func Convert(input []byte) (output bytes.Buffer, err error) {
	var notebook Notebook

	if err = json.Unmarshal(input, &notebook); err != nil {
		return
	}

	jupyterConf := &JupyterConf{
		Jupyter: Jupyter{
			Kernelspec:    notebook.Metadata.Kernelspec,
			LanguageInfo:  notebook.Metadata.LanguageInfo,
			Nbformat:      notebook.NbFormat,
			NbformatMinor: notebook.NbFormatMinor,
		},
	}

	t := template.Must(template.New("index.go.tmpl").Funcs(template.FuncMap{
		"dict": func(values ...interface{}) (map[string]interface{}, error) {
			if len(values)%2 != 0 {
				return nil, errors.New("invalid dict call")
			}
			dict := make(map[string]interface{}, len(values)/2)
			for i := 0; i < len(values); i += 2 {
				key, ok := values[i].(string)
				if !ok {
					return nil, errors.New("dict keys must be strings")
				}
				dict[key] = values[i+1]
			}
			return dict, nil
		},
		"formatError": func(value string) (string, error) {
			return FormatError(value), nil
		},
		"hasPrefix": func(value string, prefix string) (bool, error) {
			return strings.HasPrefix(value, prefix), nil
		},
	}).ParseFS(content, "templates/*.tmpl"))

	conf, _ := yaml.Marshal(jupyterConf)

	config := map[string]interface{}{
		"JupyterConfYaml": string(conf),
		"Cells":           notebook.Cells,
		"Language":        notebook.Metadata.Kernelspec.Language,
		"EmptySlice":      []string{},
	}

	if err = t.Execute(&output, config); err != nil {
		return
	}

	return
}
//

//func main() {
//
//	inputFilePath := os.Args[1]
//
//	inputFile, err := os.Open(inputFilePath)
//
//	if err != nil {
//		panic(err)
//	}
//
//	input, _ := ioutil.ReadAll(inputFile)
//
//	var tpl bytes.Buffer
//
//	tpl, err = convert(input)
//
//	if err != nil {
//		panic(err)
//	}
//
//	print(tpl.String())
//
//}