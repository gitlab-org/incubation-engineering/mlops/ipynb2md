> DEPRECATED
> Please use [ipynbdiff](https://gitlab.com/gitlab-org/incubation-engineering/mlops/ipynbdiff) instead 

# ipynb2md

Simple converter from .ipynb to markdown written in go

It is intended to be make it possible to have make diffs between 
ipynbs possible within documents. [Our aim is to replicate pandocs
conversion](examples/sample_pandoc.md)