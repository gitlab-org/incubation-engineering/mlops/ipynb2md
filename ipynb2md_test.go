package ipynb2md

import (
	"runtime/debug"
	"strings"
	"testing"
)

func buildNotebook(cells ...string) string {
	return `{
 "cells":[
`+strings.Join(cells, ",")+ `
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.6"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
`
}

func TestConvert_Metadata(t *testing.T) {

	notebook := buildNotebook()

	want := `---
jupyter:
  kernelspec:
    display_name: Python 3 (ipykernel)
    language: python
    name: python3
  language_info:
    codemirror_mode:
      name: ipython
      version: 3
    file_extension: .py
    mimetype: text/x-python
    name: python
    nbconvert_exporter: python
    pygments_lexer: ipython3
    version: 3.9.6
  nbformat: 4
  nbformat_minor: 5
---`

	got, err := Convert([]byte(notebook))

	if err != nil {
		t.Log(string(debug.Stack()))
		panic(err)
	}

	gotStr := got.String()

	if gotStr != want {
		t.Errorf("The results don't match\n Got: %s, Want: %s", gotStr, want)
	}
}