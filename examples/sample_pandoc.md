---
jupyter:
  kernelspec:
    display_name: Python 3 (ipykernel)
    language: python
    name: python3
  language_info:
    codemirror_mode:
      name: ipython
      version: 3
    file_extension: .py
    mimetype: text/x-python
    name: python
    nbconvert_exporter: python
    pygments_lexer: ipython3
    version: 3.9.6
  nbformat: 4
  nbformat_minor: 5
---

<div class="cell markdown" tags="[]">

# This is a markdown cell

This paragraph has With Many Lines. How we will he handle MR notes?

But I can add another paragraph

</div>

<div class="cell raw">

</div>

<div class="cell code" execution_count="1">

``` python
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
```

</div>

<div class="cell code" execution_count="2">

``` python
np.random.seed(42)
```

</div>

<div class="cell code" execution_count="6">

``` python
x = np.linspace(0, 4*np.pi,50)
y = np.sin(x)

plt.plot(x, y)
```

<div class="output execute_result" execution_count="6">

    [<matplotlib.lines.Line2D at 0x126db41c0>]

</div>

<div class="output display_data">

![](outputs/images/424b32b5e613544a262e99d9d19858f66583252d.png)

</div>

</div>

<div class="cell code" execution_count="7">

``` python
# A cell that has an error
y = sin(x)
```

<div class="output error" ename="NameError"
evalue="name 'sin' is not defined">

    ---------------------------------------------------------------------------
    NameError                                 Traceback (most recent call last)
    /var/folders/cq/l637k4x13gx6y9p_gfs4c_gc0000gn/T/ipykernel_72857/3962062127.py in <module>
          1 # A cell that has an error
    ----> 2 y = sin(x)

    NameError: name 'sin' is not defined

</div>

</div>

<div class="cell code" execution_count="8">

``` python
df = pd.DataFrame({"x": x, "y": y})
```

</div>

<div class="cell code" execution_count="9">

``` python
df
```

<div class="output execute_result" execution_count="9">

                x             y
    0    0.000000  0.000000e+00
    1    0.256457  2.536546e-01
    2    0.512913  4.907176e-01
    3    0.769370  6.956826e-01
    4    1.025826  8.551428e-01
    5    1.282283  9.586679e-01
    6    1.538739  9.994862e-01
    7    1.795196  9.749279e-01
    8    2.051652  8.865993e-01
    9    2.308109  7.402780e-01
    10   2.564565  5.455349e-01
    11   2.821022  3.151082e-01
    12   3.077479  6.407022e-02
    13   3.333935 -1.911586e-01
    14   3.590392 -4.338837e-01
    15   3.846848 -6.482284e-01
    16   4.103305 -8.201723e-01
    17   4.359761 -9.384684e-01
    18   4.616218 -9.953791e-01
    19   4.872674 -9.871818e-01
    20   5.129131 -9.144126e-01
    21   5.385587 -7.818315e-01
    22   5.642044 -5.981105e-01
    23   5.898500 -3.752670e-01
    24   6.154957 -1.278772e-01
    25   6.411414  1.278772e-01
    26   6.667870  3.752670e-01
    27   6.924327  5.981105e-01
    28   7.180783  7.818315e-01
    29   7.437240  9.144126e-01
    30   7.693696  9.871818e-01
    31   7.950153  9.953791e-01
    32   8.206609  9.384684e-01
    33   8.463066  8.201723e-01
    34   8.719522  6.482284e-01
    35   8.975979  4.338837e-01
    36   9.232436  1.911586e-01
    37   9.488892 -6.407022e-02
    38   9.745349 -3.151082e-01
    39  10.001805 -5.455349e-01
    40  10.258262 -7.402780e-01
    41  10.514718 -8.865993e-01
    42  10.771175 -9.749279e-01
    43  11.027631 -9.994862e-01
    44  11.284088 -9.586679e-01
    45  11.540544 -8.551428e-01
    46  11.797001 -6.956826e-01
    47  12.053458 -4.907176e-01
    48  12.309914 -2.536546e-01
    49  12.566371 -4.898587e-16

</div>

</div>

<div class="cell code">

``` python
```

</div>
